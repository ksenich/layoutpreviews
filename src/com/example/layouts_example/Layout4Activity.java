package com.example.layouts_example;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import com.example.layouts_example.dummy.RNG;
import com.example.layouts_example.model.Offer;

import java.util.ArrayList;

public class Layout4Activity extends ActionBarActivity{
//    private boolean searchExpanded = false;
//    private View menuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setProgressBarIndeterminateVisibility(true);
        setProgressBarVisibility(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_4_2);
        ListView lw = (ListView) findViewById(R.id.propos_list);
        for(int i=0;i<10;++i){
            offers.add(RNG.getRandomOffer());
        }
        lw.setAdapter(new OfferAdapter());

//        menuView = findViewById(R.id.menuReplacement);
//        menuView.setVisibility(View.INVISIBLE);
    }
    ArrayList<Offer> offers = new ArrayList<Offer>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_expanded, menu);
//        menu.findItem(R.id.action_search).setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem menuItem) {
//                searchExpanded = true;
//                invalidateOptionsMenu();
//                return false;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
//                searchExpanded = true;
//                invalidateOptionsMenu();
//                return false;
//            }
//        });
//        menuView.setVisibility(View.GONE);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
//        menuView.setVisibility(View.VISIBLE);
//        menu.clear();
//        if(searchExpanded){
//            getMenuInflater().inflate(R.menu.search_expanded, menu);
//        }else {
//            getMenuInflater().inflate(R.menu.main, menu);
//        }
        PopupWindow pw = new PopupWindow(getLayoutInflater().inflate(R.layout.layout_4_3,null));
        pw.showAtLocation(getWindow().getDecorView().findViewById(android.R.id.content),Gravity.TOP | Gravity.RIGHT,0,0);
        return true;
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
//        menuView.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_filter:{
                new FilterDialogFragment().show(getSupportFragmentManager(),"tag");
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    class OfferAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return offers.size();
        }

        @Override
        public Object getItem(int i) {
            return offers.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if(view != null){
                v=view;
            }else{
                v = getLayoutInflater().inflate(R.layout.offer_list_item_layout, null);
            }

            return v;
        }
    }
    class FilterDialogFragment extends DialogFragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v= inflater.inflate(R.layout.layout_4_6, null);

            return v;
        }
    }
    public void slideUp(View v){
        View list = findViewById(R.id.propos_list);
        list.setVisibility(View.VISIBLE);
        View handleDown = findViewById(R.id.slydeDownHandle);
        handleDown.setVisibility(View.VISIBLE);
        View handleUp = findViewById(R.id.slydeUpHandle);
        handleUp.setVisibility(View.GONE);
        View mapV = findViewById(R.id.mapHolder);
        mapV.setVisibility(View.GONE);
    }
    public void slideDown(View v){
        View list = findViewById(R.id.propos_list);
        list.setVisibility(View.GONE);
        View handleDown = findViewById(R.id.slydeDownHandle);
        handleDown.setVisibility(View.GONE);
        View handleUp = findViewById(R.id.slydeUpHandle);
        handleUp.setVisibility(View.VISIBLE);
        View mapV = findViewById(R.id.mapHolder);
        mapV.setVisibility(View.VISIBLE);
    }
}
