package com.example.layouts_example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.layouts_example.model.Offer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyDiscountsActivity extends ActionBarActivity {
    public static String DISCOUNTED_OFFERS_KEY = "discounts_on";
    ImgCache imageCache = new ImgCache();
    private List<Discount> discounts = new ArrayList<Discount>();
    private Random rng = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_discounts_page);
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            ArrayList<Offer> offers = (ArrayList<Offer>) intent.getSerializableExtra(DISCOUNTED_OFFERS_KEY);
            if (offers != null) {
                int count = 0;
                for (Offer o : offers) {
                    Discount.Status c = randomStatus();
                    Discount.Status b = randomStatus();
                    discounts.add(new Discount(c, b, o));
                    if(b == Discount.Status.OK){
                        ++count;
                    }
                }
                TextView bonuses = (TextView) findViewById(R.id.tvBonuses);
                bonuses.setText(String.valueOf(count));
                TextView offCount = (TextView) findViewById(R.id.tvOffers);
                offCount.setText(String.valueOf(offers.size()));
            }
        } else {

        }
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        ListView discountsList = (ListView) findViewById(R.id.discounts_list);
        discountsList.setAdapter(new DiscountAdapter());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.plus_button, menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Discount.Status randomStatus() {
        int r = rng.nextInt(3);
        switch (r) {
            case 0:
                return Discount.Status.FAIL;
            case 1:
                return Discount.Status.OK;
            default:
                return Discount.Status.WAIT;
        }
    }

    static class Discount {
        Status confirmed;
        Status bonuses;
        Offer offer;

        Discount(Status confirmed, Status bonuses, Offer offer) {
            this.confirmed = confirmed;
            this.bonuses = bonuses;
            this.offer = offer;
        }

        enum Status {
            OK(R.drawable.ok_status_image, R.string.discount_confirmed, R.string.bonuses_credited),
            FAIL(R.drawable.fail_status_image, R.string.discount_not_confirmed, R.string.bonuses_not_credited),
            WAIT(R.drawable.waiting_status_image, R.string.discount_waiting, R.string.bonuses_waiting),;
            int image_resource;
            int confirmation_resource;
            int bonus_resource;

            Status(int image_resource, int confirmation_resource, int bonus_resource) {
                this.image_resource = image_resource;
                this.confirmation_resource = confirmation_resource;
                this.bonus_resource = bonus_resource;
            }
        }
    }

    class DiscountAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return discounts.size();
        }

        @Override
        public Discount getItem(int i) {
            return discounts.get(i);
        }

        @Override
        public long getItemId(int i) {
            return -1;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.discount_list_item, null);
            }
            Discount discount = discounts.get(i);
            TextView confirmationStatus = (TextView) view.findViewById(R.id.tvDiscountOneStatus);
            ImageView confirmationImage = (ImageView) view.findViewById(R.id.ivDiscountOneStatus);
            confirmationStatus.setText(getString(discount.confirmed.confirmation_resource));
            confirmationImage.setImageResource(discount.confirmed.image_resource);
            TextView bonusStatus = (TextView) view.findViewById(R.id.tvBonusOneStatus);
            ImageView bonusImage = (ImageView) view.findViewById(R.id.ivBonusOneStatus);
            bonusStatus.setText(getString(discount.bonuses.bonus_resource));
            bonusImage.setImageResource(discount.bonuses.image_resource);
            ImageView image = (ImageView) view.findViewById(R.id.ivDiscountOneImage);
            imageCache.bind(discount.offer.img_url, image);
            return view;
        }
    }
}


