package com.example.layouts_example.model;

public class LatLng {
    public final double latitude,longitude;

    public LatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
