package com.example.layouts_example.model;


public class Region {
    public String name;
    public int id;
    public Country country;
    public int countryId;
    public Region(String name, int id, Country country) {
        this.name = name;
        this.id = id;
        this.country = country;
    }

    public Region(String name, int id, int countryId) {
        this.name = name;
        this.id = id;
        this.countryId = countryId;
    }

    public String toString() {
        return name;
    }
}
