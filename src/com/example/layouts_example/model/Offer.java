package com.example.layouts_example.model;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Offer implements Serializable {

    transient public Bitmap image;
    public  String name;
    public  String description;
    public  String img_url;
    public  String conditions;
    public  int price;
    public  int priceCoupon;
    public int id;
//    ForeignCollection<LatLng> locations;
    public List<LatLng> locations;
    public Category category;
    public Region region;
    public int category_id;
    public int region_id;

    public Offer() {
        this.locations = new ArrayList<LatLng>();
    }

    public Offer(int id) {
        this();
        this.id = id;
    }
}
