package com.example.layouts_example.model;

import com.example.layouts_example.R;

import java.io.Serializable;

public class Category implements Serializable {
    public int id;
    public String name;
    public boolean selected;

    public Category() {
        selected = false;
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }


    //TODO: OMG WHO WROTE THIS??????????
    public static CategoryIcon idToEnum(int category_id) {
        switch (category_id) {
            case 1:
                return CategoryIcon.AUTO;
            case 2:
                return CategoryIcon.FUN;
            case 3:
                return CategoryIcon.FOOD;
            case 4:
                return CategoryIcon.KIDS;
            case 5:
                return CategoryIcon.HEALTH;
            case 6:
                return CategoryIcon.EDUCATION;
            case 7:
                return CategoryIcon.CULTURE;
            case 8:
                return CategoryIcon.BEAUTY;
            default:
                return CategoryIcon.WHAT;
        }
    }

    public enum CategoryIcon {
        AUTO(R.drawable.pt_auto),
        BEAUTY(R.drawable.pt_beauty),
        CULTURE(R.drawable.pt_culture),
        EDUCATION(R.drawable.pt_education),
        FOOD(R.drawable.pt_food),
        FUN(R.drawable.pt_fun),
        HEALTH(R.drawable.pt_health),
        KIDS(R.drawable.pt_kids),
        WHAT(R.drawable.red_pointer);
        public int res_id;

        CategoryIcon(int res_id) {
            this.res_id = res_id;
        }
    }
}

