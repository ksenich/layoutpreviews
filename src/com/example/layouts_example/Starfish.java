package com.example.layouts_example;

import com.example.layouts_example.model.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Starfish {
    static Starfish instance;

    public Starfish() {
        cats = new ArrayList<Category>(Arrays.asList(cs));
    }

    public static Starfish getInstance() {
        if (instance == null) {
            instance = new Starfish();
        }
        return instance;
    }

    List<Category> cats;
    Category[] cs = {
            new Category(1, "Auto"),
            new Category(2, "Fun"),
            new Category(3, "Food"),
            new Category(4, "Kids"),
            new Category(5, "HEALTH"),
            new Category(6, "EDUCATION"),
            new Category(7, "CULTURE"),
            new Category(8, "BEAUTY"),
            new Category(10, "WHAT"),

    };

    public List<Category> getCategories() {
        return cats;
    }
}
