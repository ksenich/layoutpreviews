package com.example.layouts_example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class Layout1Activity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_1);
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.vk_button) == null) {
            setContentView(R.layout.layout_1);
        } else {
            super.onBackPressed();
        }
    }

    public void enter(View v) {
        setContentView(R.layout.layout_1_2);
    }

    public void register(View v) {
        setContentView(R.layout.layout_1_1);
    }
}
