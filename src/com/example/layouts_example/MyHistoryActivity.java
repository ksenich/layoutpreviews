package com.example.layouts_example;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.example.layouts_example.dummy.RNG;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MyHistoryActivity extends ActionBarActivity {
    List<HistoryItem> history = new ArrayList<HistoryItem>();
    HistoryAdapter ac = new HistoryAdapter();
    int available;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_bill_page);
        ListView historyView = (ListView) findViewById(R.id.lwUserHistoryList);
        historyView.setAdapter(new HistoryAdapter());
        for (int i = 0; i < 10; ++i) {
            history.add(new HistoryItem());
        }
        TextView couponCount = (TextView) findViewById(R.id.tvUserMoneyAmount);
        couponCount.setText(String.valueOf(available));
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    enum KIND {
        PLUS(R.color.green, R.string.not_spent),
        MINUS(R.color.red, R.string.spent),;
        int color_id;
        int comm_id;

        KIND(int style_id, int comm_id) {
            this.color_id = style_id;
            this.comm_id = comm_id;
        }
    }

    class HistoryAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return history.size();
        }

        @Override
        public Object getItem(int i) {
            return history.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if (view == null) {
                v = getLayoutInflater().inflate(R.layout.history_item_list, null);
            } else {
                v = view;
            }
            HistoryItem item = (HistoryItem) getItem(i);
            TextView comment = (TextView) v.findViewById(R.id.what);
            comment.setText(getString(item.kind.comm_id));
            TextView explanation = (TextView) v.findViewById(R.id.tvExplanation);
            explanation.setText(item.text);
            TextView amount = (TextView) v.findViewById(R.id.tvAmount);
            amount.setTextColor(item.kind.color_id);
            amount.setText(String.valueOf(item.amount));
            TextView date = (TextView) v.findViewById(R.id.tvDate);
            date.setText(item.date.getDate() + " " + item.date.getMonth());
            final HistoryItem coupon = (HistoryItem) getItem(i);
            return v;
        }

    }

    class HistoryItem {
        String text;
        KIND kind;
        int amount;
        Date date;

        HistoryItem() {
            text = RNG.randomText();
            amount = RNG.randomInt(1000);
            date = RNG.randomDate();
            kind = RNG.randomInt(2) > 1 ? KIND.PLUS : KIND.MINUS;
        }
    }
}