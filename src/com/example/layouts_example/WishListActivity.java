package com.example.layouts_example;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WishListActivity extends ActionBarActivity {
    private final Random rng = new Random();
    List<Wish> wishes = new ArrayList<Wish>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wish_list_page);
        ListView wishList = (ListView) findViewById(R.id.lwWishList);
        wishList.setAdapter(new WishAdapter());
        for (int i=0;i<16;++i){
            wishes.add(new Wish());
        }
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    class Wish{
        String title;
        String description;
        String image_url;
        int days;
        int hours;
        Wish(){
            title = "wish";
            description = "description";
            days = rng.nextInt(30);
            hours = rng.nextInt(24);
        }
    }

    class WishAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return wishes.size();
        }

        @Override
        public Object getItem(int i) {
            return wishes.get(i);
        }

        @Override
        public long getItemId(int i) {
            return -1;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if(view == null) {
                v = getLayoutInflater().inflate(R.layout.wish_list_item, null);
            }else{
                v = view;
            }
            TextView title  = (TextView)  v.findViewById(R.id.title);
            TextView hours  = (TextView)  v.findViewById(R.id.time_hours);
            TextView days   = (TextView)  v.findViewById(R.id.time_days);
            ImageView image = (ImageView) v.findViewById(R.id.image);
            Wish w = (Wish) getItem(i);
//            title.setText(w.title);
            hours.setText(String.valueOf(w.hours));
            days.setText(String.valueOf(w.days));
            return v;
        }
    }
}
