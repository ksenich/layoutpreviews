package com.example.layouts_example;


import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class EntertainmentActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entertainment_page);
        ListView ev = (ListView) findViewById(R.id.llVenuesList);
        ev.setAdapter(new EntertainmentAdapter());
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    List<Venue> venues = new ArrayList<Venue>();
    class Venue {

    }
    class EntertainmentAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return venues.size();
        }

        @Override
        public Object getItem(int i) {
            return venues.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if(view == null){
                v = getLayoutInflater().inflate(R.layout.entertainment_list_item, null);
            }else{
                v = view;
            }
            Venue venue = (Venue) getItem(i);

            return v;
        }

    }
}
