package com.example.layouts_example.dummy;

import com.example.layouts_example.model.Offer;

import java.util.Date;
import java.util.Random;

/**
 * Created by pc on 2014-03-30.
 */
public class RNG {
    public static String randomName() {
        boolean f = rng.nextBoolean();
        String first = f ? randomName(female_names) : randomName(male_names);
        String last = randomName(last_names);
        if (f && (last.endsWith("v") || last.endsWith("n"))) {
            last += "a";
        }
        return first + " " + last;
    }

    static Random rng = new Random();

    static String randomName(String[] names) {
        return names[rng.nextInt(names.length)];
    }

    static String[] male_names = {
            "Anatoly",
            "Andrei",
            "Boris",
            "Dmitriy",
            "Gennadi",
            "Grigoriy",
            "Igor",
            "Ivan",
            "Leonid",
            "Mikhail",
            "Nikolai",
            "Sergei",
            "Victor",
            "Vladimir",
            "Yuri",
    };
    static String[] female_names = {
            "Astra",
            "Galina",
            "Lyudmila",
            "Olga",
            "Tatyana",
    };
    static String[] last_names = {
            "Andianov",
            "Belov",
            "Chukarin",
            "Gorokhov",
            "Kolotov",
            "Korkin",
            "Likhachev",
            "Maleev",
            "Mikhailov",
            "Petrov",
            "Ragulin",
            "Romanov",
            "Samusenko",
            "Scharov",
            "Shadrin",
            "Shalimov",
            "Torban'",
            "Voronin",
            "Yakubik",
            "Zhdanovich",
    };

    public static String randomNick() {
        return String.valueOf(100 + rng.nextInt(900));
    }

    public static int randomInt() {
        return rng.nextInt(100);
    }

    public static int randomInt(int i) {
        return rng.nextInt(i);
    }

    public static Date randomDate() {
        return new Date(rng.nextLong());
    }

    public static String randomText() {
        return randomText(100);
    }
    public static Offer getRandomOffer(){
        Offer offer = new Offer();
        offer.category_id = randomInt(8);
        offer.conditions = randomText();
        offer.description = randomText();
//        offer.img_url = "http://"
        offer.id  = 1;
        offer.name = randomText(10);
        offer.price = randomInt();
        offer.priceCoupon = randomInt();
        return offer;

    }

    public static String randomText(int n) {
        StringBuilder sb = new StringBuilder();
        for(int i=0, e = rng.nextInt(n);i<e;++i){
            sb.append(randomName()).append(", ");
        }
        return sb.toString();
    }
}
