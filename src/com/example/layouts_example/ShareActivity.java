package com.example.layouts_example;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.layouts_example.dummy.RNG;
import com.example.layouts_example.model.Offer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShareActivity extends ActionBarActivity {
    public static final String OFFER_KEY = "com.example.starfishapp.ShareActivity.OFFER";
    Offer offer = RNG.getRandomOffer();
    int userBalance = 117;
    List<Comment> comments = new ArrayList<Comment>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_page_n);

//        Offer offer = RNG.getRandomOffer();
        setupComments();
//        if(savedInstanceState!= null){
//            offer = (Offer) savedInstanceState.getSerializable(OFFER_KEY);
//        }else{
//            offer = (Offer) getIntent().getSerializableExtra(OFFER_KEY);
//        }
        TextView conditions = (TextView) findViewById(R.id.tvShareConditions);
        conditions.setText(Html.fromHtml(offer.conditions));
        TextView price = (TextView) findViewById(R.id.tvSharePrice);
        price.setText(String.valueOf(offer.price));
        TextView coupon = (TextView) findViewById(R.id.tvShareCoupon);
        coupon.setText(String.valueOf(offer.priceCoupon));
//        ImageView image = (ImageView) findViewById(R.id.ivShareImage);
//        new ImgCache().bind(offer.img_url, image);
        TextView name = (TextView) findViewById(R.id.tvShareName);
        name.setText(offer.name);
        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayUseLogoEnabled(false);
    }

    private void setupComments() {
        for (int i = 0; i < RNG.randomInt(25); ++i) {
            comments.add(new Comment());
        }
        TextView commentsCount = (TextView) findViewById(R.id.tvCommentsCounter);
        commentsCount.setText(String.valueOf(comments.size()));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(OFFER_KEY, offer);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void buyCoupon(View v) {
        new BuyCouponDialogFragment().show(getSupportFragmentManager(), "tag");
    }

    public void showComments(View v) {
        Log.d("SHOWCOMMENTS", "HERE");
        new CommentsDialogFragment().show(getSupportFragmentManager(), "tag2");
    }

    class BuyCouponDialogFragment extends DialogFragment {
        int amount;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.buy_coupon_menu, null);
            Button buyButton = (Button) v.findViewById(R.id.bBuyFromBalance);
            buyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userBalance -= getSum();
                }
            });
            Button addButton = (Button) v.findViewById(R.id.bAddCoupon);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAmount(amount + 1);
                }
            });
            Button subButton = (Button) v.findViewById(R.id.bSubtractCoupon);
            subButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAmount(amount - 1);
                }
            });
            TextView balView = (TextView) v.findViewById(R.id.tvUserBalance);
            balView.setText(String.valueOf(userBalance));
            TextView priceView = (TextView) v.findViewById(R.id.tvCouponPrice);
            priceView.setText(String.valueOf(offer.price));
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            setAmount(2);
            super.onViewCreated(view, savedInstanceState);
        }

        private void setAmount(int i) {
            Button buyButton = (Button) getView().findViewById(R.id.bBuyFromBalance);
            amount = i;
            if(amount < 1){
                amount = 1;
            }
            TextView amView = (TextView) getView().findViewById(R.id.tvCouponNumber);
            amView.setText(String.valueOf(amount));
            TextView totalView = (TextView) getView().findViewById(R.id.tvCouponTotal);
            totalView.setText(String.valueOf(amount*offer.price));
            if(amount * offer.price > userBalance){
                buyButton.setEnabled(false);
                buyButton.setText(R.string.not_enough_money);
            }else{
                buyButton.setEnabled(true);
                buyButton.setText(R.string.buy_coupon);
            }
        }

        private int getSum() {
            return 0;
        }
    }

    class CommentsDialogFragment extends DialogFragment {

        @Override
        public View getView() {
            final View v = getLayoutInflater(null).inflate(R.layout.share_comments_page, null);
            final ListView lw = (ListView) v.findViewById(R.id.llCommentsList);
            final CommentAdapter adapter = new CommentAdapter();
            lw.setAdapter(adapter);
            Button send = (Button) v.findViewById(R.id.bSendComment);
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                    String currentTimeStamp = dateFormat.format(new Date()); // Find todays date
                    EditText et = (EditText) v.findViewById(R.id.etComment);
                    Comment c = new Comment("Me", et.getText().toString(), currentTimeStamp);
                    comments.add(c);
                    adapter.notifyDataSetChanged();
                    TextView cc = (TextView) findViewById(R.id.tvCommentsCounter);
                    cc.setText(String.valueOf(comments.size()));
                }
            });
            return v;
        }

        class CommentAdapter extends BaseAdapter {
            @Override
            public int getCount() {
                return comments.size();
            }

            @Override
            public Object getItem(int i) {
                return comments.get(i);
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View v;
                if (view == null) {
                    v = getLayoutInflater(null).inflate(R.layout.comments_list_item, null);
                } else {
                    v = view;
                }
                Comment com = (Comment) getItem(i);
                TextView content = (TextView) v.findViewById(R.id.tvCommentContent);
                content.setText(com.content);
                TextView author = (TextView) v.findViewById(R.id.tvCommentAuthor);
                author.setText(com.author);
                TextView time = (TextView) v.findViewById(R.id.tvCommentTime);
                time.setText(com.time);
                return v;
            }
        }
    }

    class Comment {
        String author;
        String content;
        String time;

        Comment() {
            author = RNG.randomName();
            content = RNG.randomText(100);
            time = RNG.randomInt(24) + ":" + RNG.randomInt(60);
        }

        public Comment(String me, String s, String time) {
            author = me;
            content = s;
            this.time = time;
        }
    }
}
