package com.example.layouts_example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final ListView lw = (ListView) findViewById(R.id.lwActivitiesList);
        lw.setAdapter(new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, previews));
        lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListActivity.this, ((Item) lw.getItemAtPosition(i)).target);
                startActivity(intent);
            }
        });
    }

    Item[] previews = {
            new Item("Layout 1",    Layout1Activity.class),
            new Item("Layout 4",    Layout4Activity.class),
            new Item("Layout 4-7",  AddOfferActivity.class),
            new Item("Layout 6-2",  EntertainmentActivity.class),
            new Item("Layout 6-3",  PointActivity.class),
            new Item("Layout 6-4",  ShareActivity.class),
            new Item("Layout 8",    MyHistoryActivity.class),
            new Item("Layout 9-1",  MyFriendsActivity.class),
            new Item("Layout 9-2",  RecomendationsActivity.class),
            new Item("Layout9-4-2", AddFriendsActivity.class),
            new Item("",            MyDiscountsActivity.class),
            new Item("Layout 10",   CouponsActivity.class),
            new Item("Layout 11",   WishListActivity.class),
            new Item("Layout 12",   AccountSettingsActivity.class),
            new Item("Layout 12-2", FavouriteCategoriesActivity.class),

    };

    class Item {
        final Class target;
        final String name;

        @Override
        public String toString() {
            return name;
        }

        Item(String name, Class target) {
            this.target = target;
            this.name = name;
        }
    }
}
