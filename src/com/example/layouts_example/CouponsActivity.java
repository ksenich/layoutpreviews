package com.example.layouts_example;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.layouts_example.model.Offer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CouponsActivity extends ActionBarActivity implements ActionBar.TabListener {
    private final Random rng = new Random();
    List<Coupon> activeCoupons = new ArrayList<Coupon>();
    CouponAdapter ac = new CouponAdapter(activeCoupons);
    List<Coupon> usedCoupons = new ArrayList<Coupon>();
    CouponAdapter uc = new CouponAdapter(usedCoupons);
    private ListView couponsView;
    private TextView couponCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_coupons_page);
        couponsView = (ListView) findViewById(R.id.lwCoupons);
        couponsView.setAdapter(new CouponAdapter(activeCoupons));
        for (int i = 0; i < 10; ++i) {
            activeCoupons.add(new Coupon());
        }
        for (int i = 0; i < 5; ++i) {
            usedCoupons.add(new Coupon());
        }
        couponCount = (TextView) findViewById(R.id.tvActiveCouponsNumber);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ab.addTab(ab.newTab()
                        .setText(getString(R.string.active))
                        .setTabListener(this)
        );
        ab.addTab(ab.newTab()
                        .setText(getString(R.string.used))
                        .setTabListener(this)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        switch(tab.getPosition()){
            case 0:{//active
                couponsView.setAdapter(ac);
                couponCount.setText(String.format(getString(R.string.active_coupons_format), activeCoupons.size()));
                break;
            }
            case 1:{//used
                couponsView.setAdapter(uc);
                couponCount.setText(String.format(getString(R.string.used_coupons_format), usedCoupons.size()));
                break;
            }
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    class Coupon {
        String code;
        String description = "";
        String name = "";
        String image_url;
        int distance;
        Offer offer;
        Coupon() {
            int cl = 16;
            StringBuilder sb = new StringBuilder();
            while (sb.length() < cl) {
                sb.append(Integer.toHexString(rng.nextInt()).toUpperCase());
            }
            sb.setLength(cl);
            code = sb.toString();
            distance = rng.nextInt(1000);
        }
    }

    class CouponAdapter extends BaseAdapter {
        List<Coupon> coupons;

        CouponAdapter(List<Coupon> c) {
            coupons = c;
        }

        @Override
        public int getCount() {
            return coupons.size();
        }

        @Override
        public Object getItem(int i) {
            return coupons.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if (view == null) {
                v = getLayoutInflater().inflate(R.layout.coupon_list_item, null);
            } else {
                v = view;
            }
            final Coupon coupon = (Coupon) getItem(i);
            TextView title = (TextView) v.findViewById(R.id.tvCouponTitle);
            TextView description = (TextView) v.findViewById(R.id.tvCouponDescription);
            ImageView image = (ImageView) v.findViewById(R.id.image);
            TextView code = (TextView) v.findViewById(R.id.tvCouponCode);
            Button copyButton = (Button) v.findViewById(R.id.bCopy);
            copyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyText(coupon);
                }
            });
            Button pathButton = (Button) v.findViewById(R.id.bShowPath);
            pathButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPath(coupon);
                }
            });
//            title.setText(coupon.name);
//            description.setText(coupon.description);
            code.setText(coupon.code);
            TextView distance = (TextView) v.findViewById(R.id.tvCouponDistance);
            distance.setText(String.valueOf(coupon.distance));
            return v;
        }

        void copyText(Coupon c){
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getString(R.string.coupon_code), c.code);
            clipboard.setPrimaryClip(clip);
        }

        void showPath(Coupon c){
//            Intent intent = new Intent(CouponsActivity.this, MapActivity.class);
//            intent.putExtra(MapActivity.END_KEY, c.offer.locations.get(0));
//            intent.putExtra(MapActivity.START_KEY, new LatLng());
//            startActivity(intent);
        }
    }
}
