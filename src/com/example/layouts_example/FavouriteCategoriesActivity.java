package com.example.layouts_example;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.layouts_example.model.Category;

import java.util.ArrayList;
import java.util.List;


public class FavouriteCategoriesActivity extends ActionBarActivity {
    List<Category> cats = new ArrayList<Category>();
    private int sellcat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_categories_page);
        ListView list = (ListView) findViewById(R.id.lwCategories);
        final CatAdapter adapter = new CatAdapter();
        list.setAdapter(adapter);
        new AsyncTask<Void,Void,List<Category>>(){
            @Override
            protected List<Category> doInBackground(Void[] objects) {
                return Starfish.getInstance().getCategories();
            }

            @Override
            protected void onPostExecute(List<Category> o) {
                cats = o;
                for(Category cat:cats){
                    if(cat.selected){
                        ++sellcat;
                    }
                }
                adapter.notifyDataSetChanged();
                setSelectedCategoriesCount(sellcat);
            }
        }.execute();
    }

    class CatAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return cats.size();
        }

        @Override
        public Object getItem(int i) {
            return cats.get(i);
        }

        @Override
        public long getItemId(int i) {
            return cats.get(i).id;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if (view == null) {
                v = getLayoutInflater().inflate(R.layout.categories_list_item, null);
            } else {
                v = view;
            }
            final Category cat = (Category) getItem(i);
            ImageView image = (ImageView) v.findViewById(R.id.ivCategoriesImage);
            image.setImageResource(Category.idToEnum(cat.id).res_id);
            CheckBox selected = (CheckBox) v.findViewById(R.id.cbCategoriesStatus);
            selected.setSelected(cat.selected);
            selected.setText(cat.name);
            selected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cat.selected = b;
                    if(cat.selected){
                        ++sellcat;
                    }else{
                        --sellcat;
                    }
                    setSelectedCategoriesCount(sellcat);
                }
            });
            return v;
        }
    }

    private void setSelectedCategoriesCount(int c) {
        TextView catCount = (TextView) findViewById(R.id.tvSelectedCategoriesCount);
        catCount.setText(Html.fromHtml(String.format(getString(R.string.selected_categories_count_format), c)));
    }
}
