package com.example.layouts_example;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.*;

public class RecomendationsActivity extends ActionBarActivity{
    List<Info> infos = new ArrayList<Info>();
    private InfoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friend_information_page);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ListView frList = (ListView) findViewById(R.id.lwFriendEventsList);
        adapter = new InfoAdapter();
        for(int i = 0;i<10;++i){
            infos.add(new Info());
        }
        frList.setAdapter(adapter);
    }

    class Info{
        String text;
        Info(){
//            text = randomText();
        }
    }
//    static Random rng = new Random();
//    static String randomText(){
//        return "";
//    }
//
//    class MarkovChain{
//        private Map<String, String[]> links = new HashMap<String, String[]>();
//
//        public String nextWord(String word){
//            return randomWord(links.get(word));
//        }
//        public String firstWord(){
//            return "";
//        }
//        String randomWord(String[] words){
//            return words[rng.nextInt(words.length)];
//        }
//        MarkovChain(){
//            links.put("",new String[]{"I", "You", "We", "They"});
//        }
//    }
    class InfoAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return infos.size();
        }

        @Override
        public Object getItem(int i) {
            return infos.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if(view == null){
                v = getLayoutInflater().inflate(R.layout.add_friend_list_item, null);
            }else{
                v = view;
            }
            Info friend = (Info) getItem(i);
//            TextView text = (TextView) v.findViewById(R.id.tvRecomendationInfo);
//            text.setText(friend.text);
            return v;
        }
    }
}
