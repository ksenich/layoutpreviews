package com.example.layouts_example;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class Layout0Activity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_0);
    }
}
