package com.example.layouts_example;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PointActivity extends ActionBarActivity {
    menuItem[] menu = {
            new menuItem(0,R.drawable.ic_offer, 17),
            new menuItem(1,R.drawable.ic_flag, 13),
            new menuItem(2,R.drawable.ic_messages, 4),
            new menuItem(3,R.drawable.ic_message, 0),
    };

     String[] names;
    public void onCreate(Bundle sis) {
        super.onCreate(sis);
        setContentView(R.layout.point_page);
        names = getResources().getStringArray(R.array.point);
        ListView menuList = (ListView) findViewById(R.id.point_menu);
        menuList.setAdapter(new ArrayAdapter<menuItem>(this, R.layout.point_menu_item, R.id.tvMenuText,menu){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                ((TextView)(view.findViewById(R.id.ivOffersNumber))).setText(String.valueOf(menu[position].itcount));
                ((ImageView)(view.findViewById(R.id.ivOfferImage))).setImageResource(menu[position].imres);
                return view;
            }
        });

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.point_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private class menuItem {
        int imres;
        int itcount;
        int pos;
        private menuItem(int pos, int imres, int itcount) {
            this.imres = imres;
            this.itcount = itcount;
            this.pos = pos;
        }

        @Override
        public String toString() {
            return names[pos];
        }
    }
}
