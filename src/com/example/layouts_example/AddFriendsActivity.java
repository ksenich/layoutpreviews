package com.example.layouts_example;


import android.app.SearchManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.layouts_example.dummy.RNG;

import java.util.ArrayList;
import java.util.List;

public class AddFriendsActivity extends ActionBarActivity {
    List<Friend> friends = new ArrayList<Friend>();
    private FriendsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friend_page_two);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ListView frList = (ListView) findViewById(R.id.lwFriendsList);
        adapter = new FriendsAdapter();
        for(int i = 0;i<10;++i){
            friends.add(new Friend());
        }
        adapter.setList(friends);
        frList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_friend, menu);
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchText(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void searchText(String text) {
        List<Friend> res = new ArrayList<Friend>();
        for(Friend f:friends){
            if(f.name.contains(text) || f.nick.contains(text)){
                //TODO:HILIGHTING
                res.add(f);
            }
        }
        adapter.setList(res);
    }

    class FriendsAdapter extends BaseAdapter {
        List<Friend> friends = new ArrayList<Friend>();

        public void setList(List<Friend> friends) {
            this.friends = friends;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return friends.size();
        }

        @Override
        public Object getItem(int i) {
            return friends.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if(view == null){
                v = getLayoutInflater().inflate(R.layout.add_friend_list_item, null);
            }else{
                v = view;
            }
            Friend friend = (Friend) getItem(i);
            TextView name = (TextView) v.findViewById(R.id.tvFriendName);
            name.setText(friend.name);
            TextView nick = (TextView) v.findViewById(R.id.tvFriendsListItemOneFriendNickname);
            nick.setText(friend.nick);
            ImageView face = (ImageView) v.findViewById(R.id.ivFriendPhoto);
//            face.setImageResource(R.drawable.ic);
            return v;
        }
    }

    private class Friend {
        public String name;
        public String nick;
        Friend(){
            name = RNG.randomName();
            nick = RNG.randomNick();
        }

    }

    private class SearchResult{
        Friend f;
        int[] nameMatches;
        int[] nickMatches;
        String text;
    }
}
